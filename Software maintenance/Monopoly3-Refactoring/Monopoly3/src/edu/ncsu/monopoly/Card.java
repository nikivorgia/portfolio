package edu.ncsu.monopoly;

public abstract class Card implements ICard {

    //attribute
    protected int cardType;

    public static int TYPE_CHANCE = 1;
    public static int TYPE_CC = 2;

    //G&S's
    @Override
    public  int getCardType() {
        return cardType;
    }
    @Override
    public void setCardType(int cardType) {
        this.cardType = cardType;
    }
}
