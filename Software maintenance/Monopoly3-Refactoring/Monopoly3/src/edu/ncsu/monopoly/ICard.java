package edu.ncsu.monopoly;

public interface ICard {
    String getLabel();

    void applyAction();

    //G&S's
    int getCardType();

    void setCardType(int cardType);
}
